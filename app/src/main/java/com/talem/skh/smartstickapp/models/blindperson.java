package com.talem.skh.smartstickapp.models;

public class blindperson {
    public String idOfBlindPerson;
    public String nameOfBlindPerson;
    public String emailOfBlindPerson;
    public String phoneOfBlindPerson;
    public double latOfBlindPerson;
    public double lonOfBlindPerson;



    public blindperson(String idOfBlindPerson, String nameOfBlindPerson, String emailOfBlindPerson, String phoneOfBlindPerson, double latOfBlindPerson, double lonOfBlindPerson) {
        this.idOfBlindPerson = idOfBlindPerson;
        this.nameOfBlindPerson = nameOfBlindPerson;
        this.emailOfBlindPerson = emailOfBlindPerson;
        this.phoneOfBlindPerson = phoneOfBlindPerson;
        this.latOfBlindPerson = latOfBlindPerson;
        this.lonOfBlindPerson = lonOfBlindPerson;
    }

    public String getIdOfBlindPerson() {
        return idOfBlindPerson;
    }

    public void setIdOfBlindPerson(String idOfBlindPerson) {
        this.idOfBlindPerson = idOfBlindPerson;
    }

    public String getNameOfBlindPerson() {
        return nameOfBlindPerson;
    }

    public void setNameOfBlindPerson(String nameOfBlindPerson) {
        this.nameOfBlindPerson = nameOfBlindPerson;
    }

    public String getEmailOfBlindPerson() {
        return emailOfBlindPerson;
    }

    public void setEmailOfBlindPerson(String emailOfBlindPerson) {
        this.emailOfBlindPerson = emailOfBlindPerson;
    }

    public String getPhoneOfBlindPerson() {
        return phoneOfBlindPerson;
    }

    public void setPhoneOfBlindPerson(String phoneOfBlindPerson) {
        this.phoneOfBlindPerson = phoneOfBlindPerson;
    }

    public double getLatOfBlindPerson() {
        return latOfBlindPerson;
    }

    public void setLatOfBlindPerson(double latOfBlindPerson) {
        this.latOfBlindPerson = latOfBlindPerson;
    }

    public double getLonOfBlindPerson() {
        return lonOfBlindPerson;
    }

    public void setLonOfBlindPerson(double lonOfBlindPerson) {
        this.lonOfBlindPerson = lonOfBlindPerson;
    }
}
