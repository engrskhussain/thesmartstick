package com.talem.skh.smartstickapp;

import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.talem.skh.smartstickapp.models.Institution;
import com.talem.skh.smartstickapp.models.blindperson;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
//apply plugin: 'com.google.gms.google-services'

    private EditText editTextName, editTextEmail, editTextPassword, editTextPhone, editTextAbout ,editTextAddress,editTextQualification;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference dbReff;
    Context c = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);

        editTextName = findViewById(R.id.edit_text_name);
        editTextEmail = findViewById(R.id.edit_text_email);
        editTextPassword = findViewById(R.id.edit_text_password);
        editTextPhone = findViewById(R.id.edit_text_phone);
        progressBar = findViewById(R.id.progressbar);
        progressBar.setVisibility(View.GONE);
        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
       // dbReff=firebaseDatabase.getReference().child("Institution");
        dbReff=firebaseDatabase.getReference().child("blindperson");
        findViewById(R.id.button_register).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mAuth.getCurrentUser() != null) {
            //handle the already login user
        }
    }

    private void registerUser() {


        final String name = editTextName.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        final String phone = editTextPhone.getText().toString().trim();


        progressBar.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password)

                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                         if (task.isSuccessful()) {
                             FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                             if (user != null) {
                                 // Name, email address, and profile photo Url
                                 String uname = user.getDisplayName();
                                 String email = user.getEmail();
                               // user.sendEmailVerification();

                                 String uid = user.getUid();
                                 blindperson i = new blindperson(
                                         uid,
                                         name,
                                         email,
                                         phone,
                                         0,
                                         0 );

                                 dbReff.child(uid).setValue(i);
                                 Intent intent;
                                 intent = new Intent(c, loginActivity.class);
                                 Toast.makeText(RegisterActivity.this, "Register sucessfull", Toast.LENGTH_LONG).show();
                                 finish();
                                 startActivity(intent);
                             }
                        } else {
                            Toast.makeText(RegisterActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_register:
                registerUser();
                break;
        }
    }
}
