package com.talem.skh.smartstickapp.models;

public class Student {


    public String idOfStudent;
    public String nameOfStudent;
    public String emailOfStudent;
    public String phoneOfStudent;
    public String qualifictionOfStudent;
    public String aboutStudent;
    // public String noOfStudents;
    //public String imageOfStudent;
    public double latOfStudent;
    public double lonOfStudent;
    public String addOfStudent;

    public Student(String idOfStudent, String nameOfStudent, String emailOfStudent, String phoneOfStudent, String qualifictionOfStudent, String aboutStudent, double latOfStudent, double lonOfStudent, String addOfStudent) {
        this.idOfStudent = idOfStudent;
        this.nameOfStudent = nameOfStudent;
        this.emailOfStudent = emailOfStudent;
        this.phoneOfStudent = phoneOfStudent;
        this.qualifictionOfStudent = qualifictionOfStudent;
        this.aboutStudent = aboutStudent;
        this.latOfStudent = latOfStudent;
        this.lonOfStudent = lonOfStudent;
        this.addOfStudent = addOfStudent;
    }

    public String getIdOfStudent() {
        return idOfStudent;
    }

    public void setIdOfStudent(String idOfStudent) {
        this.idOfStudent = idOfStudent;
    }

    public String getNameOfStudent() {
        return nameOfStudent;
    }

    public void setNameOfStudent(String nameOfStudent) {
        this.nameOfStudent = nameOfStudent;
    }

    public String getEmailOfStudent() {
        return emailOfStudent;
    }

    public void setEmailOfStudent(String emailOfStudent) {
        this.emailOfStudent = emailOfStudent;
    }

    public String getPhoneOfStudent() {
        return phoneOfStudent;
    }

    public void setPhoneOfStudent(String phoneOfStudent) {
        this.phoneOfStudent = phoneOfStudent;
    }

    public String getQualifictionOfStudent() {
        return qualifictionOfStudent;
    }

    public void setQualifictionOfStudent(String qualifictionOfStudent) {
        this.qualifictionOfStudent = qualifictionOfStudent;
    }

    public String getAboutStudent() {
        return aboutStudent;
    }

    public void setAboutStudent(String aboutStudent) {
        this.aboutStudent = aboutStudent;
    }

    public double getLatOfStudent() {
        return latOfStudent;
    }

    public void setLatOfStudent(double latOfStudent) {
        this.latOfStudent = latOfStudent;
    }

    public double getLonOfStudent() {
        return lonOfStudent;
    }

    public void setLonOfStudent(double lonOfStudent) {
        this.lonOfStudent = lonOfStudent;
    }

    public String getAddOfStudent() {
        return addOfStudent;
    }

    public void setAddOfStudent(String addOfStudent) {
        this.addOfStudent = addOfStudent;
    }
}
