package com.talem.skh.smartstickapp.models;

public class Institution {

    public String idOfInstitute;
    public String nameOfInstitute;
    public String phoneOfInstitution;
    public double latOfInstitute;
    public double lonOfInstitute;
    public String addOfInstitution;
    public String aboutInstitution;
    public String emailOfInstitution;
    public int noOfStudents;

   // public int imageOfInstitution;

    //later generate
   // public int rattingOfInstitute;


    public Institution() {
    }

    public Institution(String idOfInstitute, String nameOfInstitute, String phoneOfInstitution, double latOfInstitute, double lonOfInstitute, String addOfInstitution, String aboutInstitution, String emailOfInstitution, int noOfStudents) {
        this.idOfInstitute = idOfInstitute;
        this.nameOfInstitute = nameOfInstitute;
        this.phoneOfInstitution = phoneOfInstitution;
        this.latOfInstitute = latOfInstitute;
        this.lonOfInstitute = lonOfInstitute;
        this.addOfInstitution = addOfInstitution;
        this.aboutInstitution = aboutInstitution;
        this.emailOfInstitution = emailOfInstitution;
        this.noOfStudents = noOfStudents;
    }


    public String getPhoneOfInstitution() {
        return phoneOfInstitution;
    }

    public void setPhoneOfInstitution(String phoneOfInstitution) {
        this.phoneOfInstitution = phoneOfInstitution;
    }
    public String getNameOfInstitute() {
        return nameOfInstitute;
    }

    public void setNameOfInstitute(String nameOfInstitute) {
        this.nameOfInstitute = nameOfInstitute;
    }

    public String getAboutInstitution() {
        return aboutInstitution;
    }

    public void setAboutInstitution(String aboutInstitution) {
        this.aboutInstitution = aboutInstitution;
    }


    public int getNoOfStudents() {
        return noOfStudents;
    }

    public void setNoOfStudents(int noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    public double getLatOfInstitute() {
        return latOfInstitute;
    }

    public void setLatOfInstitute(double latOfInstitute) {
        this.latOfInstitute = latOfInstitute;
    }

    public double getLonOfInstitute() {
        return lonOfInstitute;
    }

    public void setLonOfInstitute(double lonOfInstitute) {
        this.lonOfInstitute = lonOfInstitute;
    }

    public String getIdOfInstitute() {
        return idOfInstitute;
    }

    public void setIdOfInstitute(String idOfInstitute) {
        this.idOfInstitute = idOfInstitute;
    }

    public String getAddOfInstitution() {
        return addOfInstitution;
    }

    public void setAddOfInstitution(String addOfInstitution) {
        this.addOfInstitution = addOfInstitution;
    }

    public String getEmailOfInstitution() {
        return emailOfInstitution;
    }

    public void setEmailOfInstitution(String emailOfInstitution) {
        this.emailOfInstitution = emailOfInstitution;
    }
}
