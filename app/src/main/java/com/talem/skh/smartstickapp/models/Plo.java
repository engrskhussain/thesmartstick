package com.talem.skh.smartstickapp.models;

public class Plo {

    public  String textOfPLO;
    public  String areaOfPLO;
    public String priorityOfPLO;
    public String idOfPLO;
    public String programOfPLO;

    public Plo(String textOfPLO, String areaOfPLO, String priorityOfPLO, String idOfPLO, String programOfPLO) {
        this.textOfPLO = textOfPLO;
        this.areaOfPLO = areaOfPLO;
        this.priorityOfPLO = priorityOfPLO;
        this.idOfPLO = idOfPLO;
        this.programOfPLO = programOfPLO;
    }

    public  String getTextOfPLO() {
        return textOfPLO;
    }

    public void setTextOfPLO(String textOfPLO) {
        this.textOfPLO = textOfPLO;
    }

    public  String getAreaOfPLO() {
        return areaOfPLO;
    }

    public void setAreaOfPLO(String areaOfPLO) {
        this.areaOfPLO = areaOfPLO;
    }

    public String getPriorityOfPLO() {
        return priorityOfPLO;
    }

    public void setPriorityOfPLO(String priorityOfPLO) {
        this.priorityOfPLO = priorityOfPLO;
    }

    public String getIdOfPLO() {
        return idOfPLO;
    }

    public void setIdOfPLO(String idOfPLO) {
        this.idOfPLO = idOfPLO;
    }

    public String getProgramOfPLO() {
        return programOfPLO;
    }

    public void setProgramOfPLO(String programOfPLO) {
        this.programOfPLO = programOfPLO;
    }
}
