package com.talem.skh.smartstickapp.models;


public class Program {
    public String nameOfProgram = "";
    public String codeOfProgram = "";
    public String institutionOfProgram = "";
    public String idOfProgram = "";

    public Program(String nameOfProgram, String codeOfProgram, String institutionOfProgram, String idOfProgram) {
        this.nameOfProgram = nameOfProgram;
        this.codeOfProgram = codeOfProgram;
        this.institutionOfProgram = institutionOfProgram;
        this.idOfProgram = idOfProgram;
    }
    public String getIdOfProgram() {
        return idOfProgram;
    }

    public void setIdOfProgram(String idOfProgram) {
        this.idOfProgram = idOfProgram;
    }

    public String getNameOfProgram() {
        return nameOfProgram;
    }

    public void setNameOfProgram(String nameOfProgram) {
        this.nameOfProgram = nameOfProgram;
    }

    public String getCodeOfProgram() {
        return codeOfProgram;
    }

    public void setCodeOfProgram(String codeOfProgram) {
        this.codeOfProgram = codeOfProgram;
    }

    public String getInstitutionOfProgram() {
        return institutionOfProgram;
    }

    public void setInstitutionOfProgram(String institutionOfProgram) {
        this.institutionOfProgram = institutionOfProgram;
    }
}
