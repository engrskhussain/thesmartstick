package com.talem.skh.smartstickapp.models;

import java.util.ArrayList;
import java.util.List;

public class Subject {

    public String nameOfSubject = "";
    public String codeOfSubject = "";
    public String programOfSubject = "";
    public String idOfSubject = "";
    public String institutionOfSubject = "";
    List<String> BlindPersonOfSubject = new ArrayList<>();

    public Subject(String nameOfSubject, String codeOfSubject, String programOfSubject, String idOfSubject, String institutionOfSubject, List<String> BlindPersonOfSubject) {
        this.nameOfSubject = nameOfSubject;
        this.codeOfSubject = codeOfSubject;
        this.programOfSubject = programOfSubject;
        this.idOfSubject = idOfSubject;
        this.institutionOfSubject = institutionOfSubject;
        this.BlindPersonOfSubject = BlindPersonOfSubject;
    }

    public String getNameOfSubject() {
        return nameOfSubject;
    }

    public void setNameOfSubject(String nameOfSubject) {
        this.nameOfSubject = nameOfSubject;
    }

    public String getCodeOfSubject() {
        return codeOfSubject;
    }

    public void setCodeOfSubject(String codeOfSubject) {
        this.codeOfSubject = codeOfSubject;
    }

    public String getProgramOfSubject() {
        return programOfSubject;
    }

    public void setProgramOfSubject(String programOfSubject) {
        this.programOfSubject = programOfSubject;
    }

    public String getIdOfSubject() {
        return idOfSubject;
    }

    public void setIdOfSubject(String idOfSubject) {
        this.idOfSubject = idOfSubject;
    }

    public String getInstitutionOfSubject() {
        return institutionOfSubject;
    }

    public void setInstitutionOfSubject(String institutionOfSubject) {
        this.institutionOfSubject = institutionOfSubject;
    }

    public List<String> getBlindPersonOfSubject() {
        return BlindPersonOfSubject;
    }

    public void setBlindPersonOfSubject(List<String> BlindPersonOfSubject) {
        this.BlindPersonOfSubject = BlindPersonOfSubject;
    }


}
