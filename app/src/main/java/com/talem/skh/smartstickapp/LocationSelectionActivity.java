package com.talem.skh.smartstickapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.maps.GoogleMap.OnCameraIdleListener;
import static com.google.android.gms.maps.GoogleMap.OnCameraMoveListener;

public class LocationSelectionActivity extends FragmentActivity implements OnCameraMoveListener,
        OnCameraIdleListener,
        OnMapReadyCallback,
        LocationListener {

    private GoogleMap mMap;
    Button showRouteBTN;
    EditText startingAdd, destinationAdd;
    ImageButton getLocationBtn;
    TextView pTitle;
    float defaultZoom = 15.0f;
    ArrayList<LatLng> listpoints;
    LocationManager locationManager;
    boolean locationPermission = false;
    Double cLat;
    Double cLog;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference dbReff;
    private FirebaseAuth mAuth;
    Context c = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        dbReff = firebaseDatabase.getReference().child("blindperson");

        pTitle = findViewById(R.id.ptitle);
        getLocationBtn = findViewById(R.id.imageButton);

//agr permission ni hy  tu mango warna location lena start krdo!
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
        //    ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        return;
    } else {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }



//        showRouteBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                confirmlocation();
//            }
//        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        listpoints = new ArrayList<>();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                // permission denied, boo! Disable the
// functionality that depends on this permission.
                locationPermission = grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED;
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void gotoLocationWithNameZoom(String name, float zoom) {

        Geocoder geocoder = new Geocoder(this);
        List<Address> addlist = null;
        double lat = 0;
        double lon = 0;
        try {
            addlist = geocoder.getFromLocationName(name, 1);
            if (addlist.size() == 0) {
                Toast.makeText(this, "Please make valid selection ! ", Toast.LENGTH_SHORT).show();
            } else {
                lat = addlist.get(0).getLatitude();
                lon = addlist.get(0).getLongitude();
                LatLng latLng = new LatLng(lat, lon);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
                mMap.animateCamera(cameraUpdate);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
     
     


    }

    private void gotoLocationWithLatlonZoom(double latitude, double longitude, float zoom) {

        LatLng latLng = new LatLng(latitude, longitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

            if (cLog == null) {
                gotoLocationWithLatlonZoom(24.926294, 67.022095, defaultZoom);
            } else {
                gotoLocationWithLatlonZoom(cLat, cLog, defaultZoom);
            }


    }


    @Override
    public void onCameraMove() {
        pTitle.setText("Searching...");
    }


    @Override
    public void onCameraIdle() {
        LatLng latLng = mMap.getCameraPosition().target;
        double lat = latLng.latitude;
        double lng = latLng.longitude;
        Geocoder geocoder = new Geocoder(this);
        List<Address> addlist = null;
        try {
            addlist = geocoder.getFromLocation(lat, lng, 1);
            if (addlist.size() == 0) {
                pTitle.setText("Unable to find location");
            } else {
                String country = addlist.get(0).getLocality() + " , "
                        + addlist.get(0).getCountryName() + " , "
                        + addlist.get(0).getCountryCode();
                pTitle.setText(country);
            }
        } catch (IOException e) {
            e.printStackTrace();
            pTitle.setText("Unable to find location");
        }


    }


    //////////////////////////////////////////locationcode
    void getLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            return;
        } else {

                gotoLocationWithLatlonZoom(cLat, cLog, defaultZoom);

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        cLat = location.getLatitude();
        cLog = location.getLongitude();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }


//
//    public void confirmlocation() {
//        new AlertDialog.Builder(this)
//                .setTitle("Confirm Location")
//                .setMessage("Do you want to save the pointer's location as your as the Institute's location? ")
//                .setIcon(R.drawable.mappointer)
//                .setNegativeButton("YES",
//                        new DialogInterface.OnClickListener() {
//                            boolean s1, s2 = false;
//
//                            @TargetApi(11)
//                            public void onClick(final DialogInterface dialog, int id) {
//                                //set lat lon to firebase
//                                try {
//
//                                    LatLng latLng = mMap.getCameraPosition().target;
//                                    final double lat = latLng.latitude;
//                                    final double lng = latLng.longitude;
//                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//                                    String uid = null;
//                                    if (user != null) {
//                                        uid = user.getUid();
//                                        dbReff.child(uid).child("latOfInstitute").setValue(lat).addOnSuccessListener(new OnSuccessListener<Void>() {
//                                            @Override
//                                            public void onSuccess(Void aVoid) {
//                                                s1 = true;
//                                                // Toast.makeText(LocationSelectionActivity.this, "lat added", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//                                        dbReff.child(uid).child("lonOfInstitute").setValue(lng).addOnSuccessListener(new OnSuccessListener<Void>() {
//                                            @Override
//                                            public void onSuccess(Void aVoid) {
//                                                s2 = true;
//                                                //Toast.makeText(LocationSelectionActivity.this, "long added", Toast.LENGTH_SHORT).show();
//
//                                                SharedPreferences sharedpreferences1 = getSharedPreferences("LocPrefs", Context.MODE_PRIVATE);
//                                                SharedPreferences.Editor editor1 = sharedpreferences1.edit();
//                                                editor1.putBoolean("isLocationAdded", true);
//                                                editor1.commit();
//
//                                                SharedPreferences sharedpreferences = getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);
//                                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                                editor.putString("uLat", "" + lat);
//                                                editor.putString("uLong", "" + lng);
//
//                                                Toast.makeText(LocationSelectionActivity.this, "Location Added Successfully", Toast.LENGTH_SHORT).show();
//                                                dialog.cancel();
//
//
//                                            }
//                                        });
//                                    }
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//                        })
//                .setPositiveButton("NO", new DialogInterface.OnClickListener() {
//                    @TargetApi(11)
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                }).show();
//
//    }
}
