package com.talem.skh.smartstickapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    ImageView logo, pillars, stars;
    ImageView logotext;
    ProgressBar pb;
    Animation frombottom;
    Animation fromthetop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        logo = findViewById(R.id.tlogosplash);
        pillars = findViewById(R.id.pillarssplash);
        stars = findViewById(R.id.starssplash);
        pb = findViewById(R.id.progressBar2);
        logotext = findViewById(R.id.talemtxtsplash);
        frombottom = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        fromthetop = AnimationUtils.loadAnimation(this, R.anim.fromthetop);
        //logo.setAnimation(fromthetop);
        stars.setAnimation(fromthetop);
        //logotext.setAnimation(fromthetop);
        //pb.setAnimation(frombottom);
        pillars.setAnimation(frombottom);

        // Start lengthy operation in a background thread
        new Thread(new Runnable() {
            public void run() {
                doWork();
                startApp();
                finish();
            }
        }).start();
    }

    private void doWork() {
        for (int progress = 50; progress < 100; progress += 20) {
            try {
                Thread.sleep(1000);
                pb.setProgress(progress);
            } catch (Exception e) {
                e.printStackTrace();
                //Timber.e(e.getMessage());
            }
        }
    }

    private void startApp() {
        Intent intent = new Intent(SplashScreen.this, loginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}



