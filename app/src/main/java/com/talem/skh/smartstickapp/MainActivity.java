package com.talem.skh.smartstickapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkEmail();
    }

    public  void  checkEmail(){
        user = FirebaseAuth.getInstance().getCurrentUser();
        // Name, email address, and profile photo Url
        String name = user.getDisplayName();
        String email = user.getEmail();
        String uid = user.getUid();


        // Check if user's email is verified
        boolean emailVerified = user.isEmailVerified();
        if(emailVerified==false){
            user.sendEmailVerification();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this,R.style.Theme_AppCompat_Light_Dialog);
            alertDialogBuilder.setTitle("Login Failed");
            alertDialogBuilder.setCancelable(false);
            String message = "Your email address is not verified please check your mail box or press RESEND to resend";
            alertDialogBuilder.setMessage(Html.fromHtml(message));
            alertDialogBuilder.setNegativeButton("CANCEL",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finish();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();


        }
    }

}
